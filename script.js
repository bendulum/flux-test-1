(function(win) {

    var $ = win.jQuery;
    var _ = win._;
    var id = 0;

    /** Actions */

    win.fActions = {

        create: function(text) {
            win.fDispatcher.trigger('CREATE', {
                text: text,
                date: new Date().getTime()
            });
        },

        delete: function(id) {
            win.fDispatcher.trigger('DELETE', {
                id: id
            });
        },

        deleteAll: function() {
            win.fDispatcher.trigger('DELETE_ALL');
        },

        update: function(attrs) {
            win.fDispatcher.trigger('UPDATE', attrs);
        }

    };


    /** Dispatcher */

    win.fDispatcher = _.extend({}, win.fEvents);


    /** Store */

    win.fStore = _.extend({}, {

        items: {},

        create: function(attrs) {
            attrs.id = id++;
            attrs.dragging = false;
            attrs.sequence = this.getSequenceEnd();
            this.items[attrs.id] = attrs;
            this.trigger('CREATE', this.items);
        },

        delete: function(id) {
            if (this.items[id]) delete this.items[id];
            this.trigger('DELETE', this.items);
        },

        get: function(id) {
            return this.items[id];
        },

        getAll: function() {
            return this.items;
        },

        getSequenceEnd: function() {
            return (_.last(_.pluck(this.items, 'sequence').sort()) || 0) + 1;
        },

        deleteAll: function() {
            for (var id in this.items) {
                if (this.items[id]) delete this.items[id];
            }
            this.trigger('DELETE_ALL', this.items);
        },

        update: function(attrs) {
            var current = _.clone(this.items[attrs.id]);
            this.items[attrs.id] = _.extend(current, attrs);
            this.trigger('UPDATE', this.items);
        }

    }, win.fEvents);

    win.fStore.listenTo(win.fDispatcher, 'all', function(event, payload) {
        switch(event) {
            case 'CREATE': win.fStore.create(payload); break;
            case 'DELETE': win.fStore.delete(payload.id); break;
            case 'DELETE_ALL': win.fStore.deleteAll(); break;
            case 'UPDATE': win.fStore.update(payload); break;
        }
    });


    /** View */

    win.fView = _.extend({}, {

        init: function() {
            this.html = '';
            this.setData(win.fStore.getAll());
            this.bindStore();
            this.bindUI();
            this.bindSortable();
        },

        bindStore: function() {
            this.listenTo(win.fStore, 'all', _.bind(this.onChange, this));
        },

        onChange: function() {
            this.setData(win.fStore.getAll());
            this.render();
        },

        setData: function(obj) {
            this.items = _.sortBy(obj, function(item) {
                return item.sequence;
            });
        },

        bindUI: function() {
            $('#view')
                .on('click', 'span.delete', function(e) {
                    var $item = $(e.currentTarget).closest('.item-outer');
                    var id = parseInt($item.attr('data-id'));
                    console.log('view delete', id, $item);
                    win.fActions.delete(id);
                })
                .on('click', 'div.clear', function(e) {
                    console.log('view delete all');
                    win.fActions.deleteAll();
                });
        },

        bindSortable: function() {

        },

        render: function() {
            var html = _.map(this.items, function(item) {
                if (item.dragging === true) return '';
                return ('<div class="item-outer" data-id="'+ item.id +'"><div class="item" style="padding: 5px;">' +
                    item.text + ' <span style="font-size: 10px;" class="delete">(delete)</span>' +
                    //'<span style="font-size: 9px;">seq:' + item.sequence + ' id:' + item.id + '</span>' +
                    '</div></div>'
                );
            }).join('');
            html = '<div class="wrapper">' + html;
            html += '</div>';
            var node = $('#view .wrapper')[0];

            win.fMorph(node, html, {
                childrenOnly: true,
                onNodeDiscarded: function() {
                    console.log('morph discard node');
                }
            });
        }

    }, win.fEvents);
    win.fView.init();
    win.fView.render();


    /** Sortable */

    var idFromDom = function(el) {
        return parseInt($(el).attr('data-id'));
    };

    var siblingDom = function(el, direction) {
        var sequence = null;
        try {
            var $rows = $(el)[direction + 'All']('.item-outer');
            $rows = $rows.filter(function() {
                return (
                    $(this).is(':visible') &&
                    !$(this).hasClass('ui-sortable-helper') &&
                    !$(this).hasClass('is-dragged') &&
                    $(this).hasClass('item-outer')
                )
            });
            var id = $rows.first().attr('data-id');
            if (!id) return null;
            id = parseInt(id);
            sequence = win.fStore.get(id).sequence;
        } catch (e) {
            throw e;
        }
        return sequence;
    };

    var seqFromDom = function(el) {
        var before = siblingDom(el, 'prev');
        var after = siblingDom(el, 'next');
        var sequence = null;
        if (before && after) {
            sequence = (before + after) / 2;
        } else if (before && !after) {
            sequence = before + 1;
        } else if (after && !before) {
            sequence = after - 1;
        }
        return sequence;
    };

    var updateProps = {};

    $('#view').sortable({
        items: '.item-outer:not(.clear)',
        placeholder: {
            element: function(currentItem) {
                return $('<div>------------------</div>')[0];
            },
            update: function(container, p) {
                return;
            }
        },
        forcePlaceholderSize: true,
        forceHelperSize: true,
        helper: function(e, el) {
            var helper = $(el).clone();
            return helper[0];
        },
        start: function(e, ui) {
            $(ui.item)
                .addClass('is-dragged')
                .css('display', 'none')
                .css('visibility', 'hidden');
        },
        beforeStop: function(e, ui) {
            var id = idFromDom(ui.helper);
            var seq = seqFromDom(ui.placeholder);
            updateProps = {id: id, sequence: seq};
        },
        stop: function(e, ui) {
            win.fActions.update(updateProps);
            updateProps = {};
        }
    });


    /** Input */

    $('#input').on('keyup', function(e) {
        e.preventDefault();
        var $input = $(e.target);
        var val = $input.val();
        if (e.which === 13 && val.length > 0) {
            fActions.create(val);
            $input.val('');
        }
    });


    /** Bootstrap */

    for (var i = 0; i < 10; i++) {
        win.fActions.create('item_' + i);
    }

    /** Debugger */

    win.fDebugger = _.extend({}, win.fEvents);
    win.fDebugger.listenTo(win.fDispatcher, 'CREATE', function() {
        console.log('dispatcher create', arguments);
    });
    win.fDebugger.listenTo(win.fDispatcher, 'DELETE', function() {
        console.log('dispatcher delete', arguments);
    });
    win.fDebugger.listenTo(win.fDispatcher, 'UPDATE', function() {
        console.log('dispatcher update', arguments);
    });

})(window);